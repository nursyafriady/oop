<?php
    require('animal.php');
    require('Frog.php');
    require('Ape.php');
    
    $sheep = new Animal("shaun");

    echo "<br><br>";
    echo "Name of Animal is : " . $sheep->name . "<br>"; // "shaun"
    echo "Total legs are : " . $sheep->legs . "<br>"; // 2
    echo "Cold Blooded is : " . $sheep->cold_blooded . "<br><br>"; // false

    $kodok = new Frog("buduk");
    echo "Name of Animal is : " . $kodok->name . "<br>"; 
    echo "Total legs are : " . $kodok->legs . "<br>"; 
    echo "Cold Blooded is : " . $kodok->cold_blooded . "<br>"; 
    echo "Sound when jump : ";
    echo $kodok->jump() . "<br><br>"; // "hop hop"

    $sungokong = new Ape("kera sakti");
    echo "Name of Animal is : " . $sungokong->name . "<br>";
    echo "Total legs are : " . $sungokong->legs . "<br>";
    echo "Cold Blooded is : " . $sungokong->cold_blooded . "<br>"; 
    echo "Sound when Yell : ";
    $sungokong->yell() // "Auooo"    
?>